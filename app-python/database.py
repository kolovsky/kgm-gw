import psycopg2

connection = psycopg2.connect(host="host", user="user", password="password", database="database")
cursor = connection.cursor()
cursor.execute("SELECT id, ST_asgeojson(geometry) as geojson FROM orp LIMIT 10")
for row in cursor.fetchall():
    print row
connection.close()
