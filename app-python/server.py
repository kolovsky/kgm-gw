from flask import Flask, Response
from flask import request
app = Flask(__name__)


def my_response(content, json=False):
    if json:
        return Response(content, headers={"Access-Control-Allow-Origin": "*", "Content-Type": "application/json"})
    return Response(content, headers={"Access-Control-Allow-Origin": "*"})


@app.route('/')
def hello_world():
    return my_response("This is my Geo app!")


@app.route('/data')
def data():
    where = ""
    if "where" in request.args:
        where = request.args["where"]
    return my_response("{\"message\":\"This is my Geo app!\", \"where\": \"%s\"}" % where, True)


if __name__ == "__main__":
    app.debug = True
    app.run()
