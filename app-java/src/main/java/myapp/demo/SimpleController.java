package myapp.demo;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class SimpleController {
    @GetMapping(value = {"/"})
    public String main(){
        return "My Geo app!";
    }

    @GetMapping(value = {"/data"}, produces = {"application/json"})
    public String data(@RequestParam(value = "where", defaultValue = "") String where){
        return "{\"message\": \"My Geo app!\", \"where\": \""+where+"\"}";
    }
}
