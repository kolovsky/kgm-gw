package myapp.demo;

import org.junit.Test;

import java.sql.*;

public class DatabaseTest {
    @Test
    public void database() throws SQLException {
        String url = "jdbc:postgresql://host/database?user=username&password=password";
        Connection conn = DriverManager.getConnection(url);
        PreparedStatement st = conn.prepareStatement("SELECT id, ST_asgeojson(geometry) as geojson FROM orp LIMIT 10");
        ResultSet rs = st.executeQuery();
        while(rs.next()){
            int id = rs.getInt("id");
            String geom = rs.getString("geojson");
            System.out.println(id+", "+geom);
        }
    }
}
